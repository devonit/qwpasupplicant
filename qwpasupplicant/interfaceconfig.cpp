/*
*
*/

#include <QStringList>
#include "configuration.h"

namespace QWpaSupplicant
{
/* static members */
// THIS STRING LIST IS MATCHED TO THE DEFINITION OF THE KnownDriver ENUM!
QStringList InterfaceConfig::s_driverStringList = QStringList()
        << "wired"
        << "nl80211"
        << "wext"
        << "hostap"
        << "hermese"
        << "madwifi"
        << "atmel"
        << "ndiswrapper"
        << "broadcom"
        << "ipw"
        << "roboswitch"
        << "bsd"
        << "ndis";

QString InterfaceConfig::driverToString(KnownDriver driver)
{
    return s_driverStringList.at(driver);
}

InterfaceConfig::KnownDriver InterfaceConfig::stringToDriver(const QString &driverName)
{
    KnownDriver driver;
    if(s_driverStringList.indexOf(driverName) >= 0)
       driver = KnownDriver(s_driverStringList.indexOf(driverName));

    return driver;
}

/* instance members */
InterfaceConfig::InterfaceConfig(const QString &ifname) :
    m_apScan(ApScanNative),
    m_fastReauth(true)
{
    insert("Ifname",ifname);
}
InterfaceConfig::InterfaceConfig() :
    m_apScan(ApScanNative),
    m_fastReauth(true)
{
}

InterfaceConfig::KnownDriver InterfaceConfig::driver() const
{
    return stringToDriver(value("Driver").toString());
}

void InterfaceConfig::setDriver(KnownDriver driver)
{
    insert("Driver",driverToString(driver));
    if(driver == DriverWired)
        setWiredRequirements();
}

void InterfaceConfig::setWiredRequirements()
{
    setApScan(ApScanDriver);
}

QString InterfaceConfig::ifname() const
{
    return value("Ifname").toString();
}

void  InterfaceConfig::setIfname(const QString &ifname)
{
    if(!ifname.isEmpty()){
        insert("Ifname",ifname);
    }
}

QString InterfaceConfig::bridgeIfname() const
{
    return value("bridgeIfname").toString();
}

void  InterfaceConfig::setBridgeIfname(const QString &bridgeIfname)
{
    if(!bridgeIfname.isEmpty()){
        insert("Bridge_ifname",bridgeIfname);
    }
}

ApScanPolicy InterfaceConfig::apScan() const
{
    return m_apScan;
}

void InterfaceConfig::setApScan(ApScanPolicy apScan)
{
    m_apScan = apScan;
}

bool InterfaceConfig::fastReauth() const
{
    return m_fastReauth;
}

void InterfaceConfig::setFastReauth(bool enabled)
{
    m_fastReauth = enabled;
}

QString InterfaceConfig::country() const
{
    return m_country;
}

void InterfaceConfig::setCountry(const QString &country)
{
    if(!country.isEmpty()){
        m_country = country;
    }
}
} // end of namespace
// EOF
