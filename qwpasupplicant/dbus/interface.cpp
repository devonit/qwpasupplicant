 /* Command line was: qdbusxml2cpp -v -p WpaSupplicantInterfaceIf.h:WpaSupplicantInterface.cpp -N fi.w1.wpa_supplicant1.Interface.xml
 *
 * qdbusxml2cpp is Copyright (C) 2014 Digia Plc and/or its subsidiary(-ies).
 *
 * This is an auto-generated file.
 * This file may have been hand-edited. Look for HAND-EDIT comments
 * before re-generating it.
 */

#include "interface.h"

/*
 * Implementation of interface class FiW1Wpa_supplicant1InterfaceInterface
 */

FiW1Wpa_supplicant1InterfaceInterface::FiW1Wpa_supplicant1InterfaceInterface(const QString &service, const QString &path, const QDBusConnection &connection, QObject *parent)
    : QDBusAbstractInterface(service, path, staticInterfaceName(), connection, parent)
{
}

FiW1Wpa_supplicant1InterfaceInterface::~FiW1Wpa_supplicant1InterfaceInterface()
{
}

