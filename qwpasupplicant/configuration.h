
#ifndef QWPASUPPLICANT_H
#define QWPASUPPLICANT_H

#include <QVariantMap>

namespace QWpaSupplicant {


// MAC address policy
// 0 = use permanent MAC address
// 1 = use random MAC address for each ESS connection
// 2 = like 1, but maintain OUI (with local admin bit set)
enum MacAddressPolicy {
    MacAddressPermanent,
    MacAddressRandom,
    MacAddressRandomOui
};

// ocsp: Whether to use/require OCSP to check server certificate
//	0 = do not use OCSP stapling (TLS certificate status extension)
//	1 = try to use OCSP stapling, but not require response
//	2 = require valid OCSP stapling response
enum OcspPolicy {
    OcspNone,
    OcspTry,
    OcspRequire
};

// AP scanning/selection
// By default, wpa_supplicant requests driver to perform AP scanning and then
// uses the scan results to select a suitable AP. Another alternative is to
// allow the driver to take care of AP scanning and selection and use
// wpa_supplicant just to process EAPOL frames based on IEEE 802.11 association
// information from the driver.
// 1: wpa_supplicant initiates scanning and AP selection; if no APs matching to
//    the currently enabled networks are found, a new network (IBSS or AP mode
//    operation) may be initialized (if configured) (default)
// 0: driver takes care of scanning, AP selection, and IEEE 802.11 association
//    parameters (e.g., WPA IE generation); this mode can also be used with
//    non-WPA drivers when using IEEE 802.1X mode; do not try to associate with
//    APs (i.e., external program needs to control association). This mode must
//    also be used when using wired Ethernet drivers.
//    Note: macsec_qca driver is one type of Ethernet driver which implements
//    macsec feature.
// 2: like 0, but associate with APs using security policy and SSID (but not
//    BSSID); this can be used, e.g., with ndiswrapper and NDIS drivers to
//    enable operation with hidden SSIDs and optimized roaming; in this mode,
//    the network blocks in the configuration file are tried one by one until
//    the driver reports successful association; each network block should have
//    explicit security policy (i.e., only one option in the lists) for
//    key_mgmt, pairwise, group, proto variables
enum ApScanPolicy {
    ApScanDriver,
    ApScanNative,
    ApScanDriverAssociate
};

// Set doc/wpa_supplicant/conf/interface_block.txt
class InterfaceConfig : public QVariantMap
{
public:
    InterfaceConfig(const QString &ifname);
    InterfaceConfig();

    /* Drivers Allowed by wpa_supplicant:
     * hostap: (default) Host AP driver (Intersil Prism2/2.5/3). (this can also be used with Linuxant DriverLoader).
     * hermes: Agere Systems Inc. driver (Hermes-I/Hermes-II).
     * madwifi: MADWIFI 802.11 support (Atheros, etc.).
     * atmel: ATMEL AT76C5XXx (USB, PCMCIA).
     * wext: Linux wireless extensions (generic).
     * ndiswrapper: Linux ndiswrapper.
     * broadcom: Broadcom wl.o driver.
     * ipw: Intel ipw2100/2200 driver.
     * wired: wpa_supplicant wired Ethernet driver
     * roboswitch: wpa_supplicant Broadcom switch driver
     * bsd :BSD 802.11 support (Atheros, etc.).
     * ndis: Windows NDIS driver.
     *
     * NOTE:
     *      IF YOU CHANGE THIS UPDATE THE STRINGLIST
     */
    enum KnownDriver {
        DriverWired,
        DriverNl80211,
        DriverWExt,
        DriverHostAp,
        DriverHermese,
        DriverMadWifi,
        DriverAtmel,
        DriverNdisWrapper,
        DriverBroadcom,
        DriverIpw,
        DriverRoboswitch,
        DriverBsd,
        DriverNdis
    };

    static QString driverToString(KnownDriver driver);
    static KnownDriver stringToDriver(const QString &driver);

    // These are used with
    // fi.w1.wpa_supplicant1::wpa_supplicant1.CreateInterface
    KnownDriver driver() const;
    void setDriver(KnownDriver driver);

    QString ifname() const;
    void setIfname(const QString &ifname);

    QString bridgeIfname() const;
    void setBridgeIfname(const QString &bridgeIfname);

    // this is not in the conf, but IS in the dbus handler code.
    QString configFile();
    void setConfigFile(const QString &configFile);


    // These are used afterward. They aren't allowed to be set @ Create...
    // fi.w1.wpa_supplicant1::interface.apScan(uint)
    ApScanPolicy apScan() const;
    void setApScan(ApScanPolicy apScan);

    // fi.w1.wpa_supplicant1::interface.fastReauth(bool)
    bool fastReauth() const;
    void setFastReauth(bool enabled);

    // fi.w1.wpa_supplicant1::interface.country(string)
    QString country() const;
    void setCountry(const QString &country);

private:
    static int s_interfaceConfigMetaTypeId;
    ApScanPolicy m_apScan; // defaults NATIVE
    bool m_fastReauth; // defaults true
    QString m_country; // defaults to US
    static QStringList s_driverStringList;

    void setWiredRequirements();
};



// Set doc/wpa_supplicant/conf/network_block.txt
class NetworkConfig : public QVariantMap
{
public:
    NetworkConfig();

    bool enabled() const;
    void setEnabled(bool enabled);

    QString idStr() const;
    void setIdStr(const QString &idStr);

    QString ssid() const;
    void setSsid(const QString &ssid);

    bool scanSsid() const;
    void setScanSsid(bool scanSsid);

    QString bssid() const;
    void setBssid(const QString &bssid);

    int priority() const;
    void setPriority(int priority);

    int mode() const;
    void setMode(int mode);

    int frequency() const;
    void setFrequency(int frequency);

    // scan_freq
    // freq_list
    // bgscan

    enum ProtocolSettings {
        ProtocolWpa = 1,
        ProtocolRsn = 2
    };
    Q_DECLARE_FLAGS(Protocol,ProtocolSettings)

    Protocol proto() const;
    void setProto(Protocol proto);

    enum KeyManagementSettings {
        KeyWpaPsk = 1,
        KeyWpaEap = 2,
        KeyIeee8021X = 4,
        KeyNone = 8,
        KeyWpaPskSha256 = 16,
        KeyWpaEapSha256 = 32
    };
    Q_DECLARE_FLAGS(KeyManagement,KeyManagementSettings)

    KeyManagement keyMgmt() const;
    void setKeyMgmt(KeyManagement keyMgmt);

    // ieee80211w

    enum AuthenticationAlgorithmSettings {
        AuthOpen = 1,
        AuthShared = 2,
        AuthLeap = 4
    };
    Q_DECLARE_FLAGS(AuthenticationAlgorithm,AuthenticationAlgorithmSettings)

    AuthenticationAlgorithm authAlg() const;
    void setAuthAlg(AuthenticationAlgorithm authAlg);

    enum PairwiseSettings {
        PairwiseCcmp = 1,
        PairwiseTkip = 2,
        PairwiseNone = 4 // this option is deprecated.
    };
    Q_DECLARE_FLAGS(Pairwise,PairwiseSettings)

    Pairwise pairwise() const;
    void setPairwise(Pairwise pairwise);

    enum GroupCipherSettings {
        GroupCipherCcmp = 1,
        GroupCipherTkip = 2,
        GroupCipherWep104 = 4,
        GroupCipherWep40 = 8
    };
    Q_DECLARE_FLAGS(GroupCipher,GroupCipherSettings)

    GroupCipher group() const;
    void setGroup(GroupCipher group);

    QString psk() const;
    void setPsk(const QString &psk);

    // eapol_flags: IEEE 802.1X/EAPOL options (bit field)
    // Dynamic WEP key required for non-WPA mode
    // bit0 (1): require dynamically generated unicast WEP key
    // bit1 (2): require dynamically generated broadcast WEP key
    // 	(3 = require both keys; default)
    // Note: When using wired authentication (including macsec_qca driver),
    // eapol_flags must be set to 0 for the authentication to be completed
    // successfully.
    enum EapolFlags {
        EapolNone,       // 00
        EapolUnicase,    // 01
        EapolBroadcast,  // 10
        EapolBoth        // 11
    };

    EapolFlags eapolFlags() const;
    void setEapolFlags(EapolFlags eapolFlags);

    // macsec_policy
    // mixed_cell
    // proactive_key_caching

    QString wepKey0() const;
    void setWepKey0(const QString &wepKey0);

    QString wepKey1() const;
    void setWepKey1(const QString &wepKey1);

    QString wepKey2() const;
    void setWepKey2(const QString &wepKey2);

    QString wepKey3() const;
    void setWepKey3(const QString &wepKey3);

    uint wepTxKeyIdx() const;
    void setWepTxKeyIdx(uint wepTxKeyIdx);

    bool peerkey() const;
    void setPeerkey(bool peerkey);

    int wpaPtkRekey() const;
    void setWpaPtkRekey(int wpaPtkRekey);

    enum EapMethodSettings {
        EapMethodMd5 = 1,
        EapMethodMsChapV2 = 2,
        EapMethodOtp = 4,
        EapMethodGtc = 8,
        EapMethodTls = 16,
        EapMethodPeap = 32,
        EapMethodTtls = 64
    };
    Q_DECLARE_FLAGS(EapMethod,EapMethodSettings)

    EapMethod eap() const;
    void setEap(EapMethod eap);

    QString identity() const;
    void setIdentity(const QString &identity);

    QString anonymousIdentity() const;
    void setAnonymousIdentity(const QString &anonymousIdentity);

    QString password() const;
    void setPassword(const QString &password);

    QString caCert() const;
    void setCaCert(const QString &caCert);

    QString caPath() const;
    void setCaPath(const QString &caPath);

    QString clientCert() const;
    void setClientCert(const QString &clientCert);

    QString privateKey() const;
    void setPrivateKey(const QString &privateKey);

    QString privateKeyPassword() const;
    void setPrivateKeyPassword(const QString &privateKeyPassword);

    QString dhFile() const;
    void setDhFile(const QString &dhFile);

    QString subjectMatch() const;
    void setSubjectMatch(const QString &subjectMatch);

    QString altSubjectMatch() const;
    void setAltSubjectMatch(const QString &altSubjectMatch);

    QString phase1() const;
    void setPhase1(const QString &phase1);

    QString phase2() const;
    void setPhase2(const QString &phase2);

    // tls_allow_md5
    // tls_disable_time_checks
    // tls_disable_session_ticket
    // tls_disable_tlsv1_1
    // tls_disable_tlsv1_2

    // These are used for inner Phase2 auth e/ EAP-TTLS/EAP-PEAP
    QString caCert2() const;
    void setCaCert2(const QString &caCert);

    QString caPath2() const;
    void setCaPath2(const QString &caPath);

    QString clientCert2() const;
    void setClientCert2(const QString &clientCert);

    QString privateKey2() const;
    void setPrivateKey2(const QString &privateKey);

    QString privateKey2Password() const;
    void setPrivateKey2Password(const QString &privateKeyPassword);

    QString dhFile2() const;
    void setDhFile2(const QString &dhFile);

    QString subjectMatch2() const;
    void setSubjectMatch2(const QString &subjectMatch);

    QString altSubjectMatch2() const;
    void setAltSubjectMatch2(const QString &altSubjectMatch);

    uint fragementSize() const;
    void setFragmentSize(uint fragmentSize);

    OcspPolicy ocsp() const;
    void setOcsp(OcspPolicy ocsp);

    QString openSslCiphers() const;
    void setOpenSslCiphers(const QString &ciphers);

    bool erp() const;
    void setErp(bool enabled);

    // EAP-FAST variables:
    QString pacFile() const;
    void setPacFile(const QString &pacFile);
    // EAP-FAST has it's own phase1 .. but I fail in understanding.
    // EAP-FAST: fast_max_pac_list_len
    // EAP-FAST: fast_pac_format

    MacAddressPolicy macAddr() const;
    void setMacAddr(MacAddressPolicy macAddrPolicy);

    bool disableHt() const;
    void setDisableHt(bool disable);

    bool disableHt40() const;
    void setDisableHt40(bool disable);

    bool disableSgi() const;
    void setDisableSgi(bool disable);

    bool disableLpdc() const;
    void setDisableLpdc(bool disable);

    bool ht40Intolerant() const;
    void setHt40Intolerant(bool enable);

    // ht_mcs : Parsed as an array of bytes, in base-16 (ascii-hex) [later]
    // disable_max_amsdu:  Whether MAX_AMSDU should be disabled.
    // ampdu_factor: Maximum A-MPDU Length Exponent
    // ampdu_density:  Allow overriding AMPDU density configuration

    bool disableVht() const;
    void setDisableVht(bool disable);

    // vht_capa: VHT capabilities to set in the override
    // vht_capa_mask: mask of VHT capabilities
    // vht_rx_mcs_nss_1/2/3/4/5/6/7/8: override the MCS set for RX NSS 1-8
    // vht_tx_mcs_nss_1/2/3/4/5/6/7/8: override the MCS set for TX NSS 1-8

private:
    static int s_networkConfigMetaTypeId;
};

/*
 * We'll come back to this.
 *
class CredentialsConfig : QVariantMap
{
public:
    CredentialsConfig();

    QString excludedSsid() const;
    void setExcludedSsid(const QString &excludedSsid);

    QString roamingPartner() const;
    void setRoamingPartner(const QString &roamingPartner);

    // update_identifier
    // provisioning_sp
    // min_dl_bandwidth_home
    // min_ul_bandwidth_home
    // min_dl_bandwidth_roaming
    // min_ul_bandwidth_roaming
    // max_bss_load: Maximum BSS Load Channel Utilization (1..255)
    // req_conn_capab

    uint ocsp() const;
    void setOcsp(uint ocsp);

    // sim_num
    // hs20
private:
    static int s_credentialsConfigMetaTypeId;
};
*/

Q_DECLARE_OPERATORS_FOR_FLAGS(NetworkConfig::Protocol)
Q_DECLARE_OPERATORS_FOR_FLAGS(NetworkConfig::KeyManagement)
Q_DECLARE_OPERATORS_FOR_FLAGS(NetworkConfig::AuthenticationAlgorithm)
Q_DECLARE_OPERATORS_FOR_FLAGS(NetworkConfig::Pairwise)
Q_DECLARE_OPERATORS_FOR_FLAGS(NetworkConfig::GroupCipher)
Q_DECLARE_OPERATORS_FOR_FLAGS(NetworkConfig::EapMethod)


} // end namespace WpaSupplicant

/// declare the metatypes
Q_DECLARE_METATYPE(QWpaSupplicant::InterfaceConfig);
Q_DECLARE_METATYPE(QWpaSupplicant::NetworkConfig);

#endif // QWPASUPPLICANT_H
