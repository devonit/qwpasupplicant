include(../qwpasupplicant.pri)


TEMPLATE = lib
TARGET = qwpasupplicant
DESTDIR = ../lib
DEPENDPATH += .
INCLUDEPATH += .
CONFIG += $${QWPASUPPLICANT_LIBRARY_TYPE}
VERSION = $${QWPASUPPLICANT_VERSION}
QT += dbus

# input
HEADERS = \
    $${PWD}/configuration.h \
    $${PWD}/dbus/wpasupplicant.h \
    $${PWD}/dbus/interface.h \
    $${PWD}/dbus/network.h

SOURCES = \
    $${PWD}/networkconfig.cpp \
    $${PWD}/interfaceconfig.cpp \
    $${PWD}/dbus/wpasupplicant.cpp \
    $${PWD}/dbus/interface.cpp \
    $${PWD}/dbus/network.cpp

target.path = $${PREFIX}/$${LIBDIR}
header_files.files = $${HEADERS}
header_files.path = $${PREFIX}/include/qwpasupplicant
INSTALLS += target header_files

# pkg-config support
CONFIG += create_pc create_prl no_install_prl

QMAKE_PKGCONFIG_NAME = qwpasupplicant
QMAKE_PKGCONFIG_DESCRIPTION = QWpaSupplicant provides a Qt wrapper around wpa_supplicant\'s dbus interface
QMAKE_PKGCONFIG_DESTDIR = pkgconfig
QMAKE_PKGCONFIG_LIBDIR = $$target.path
QMAKE_PKGCONFIG_INCDIR = $$header_files.path
equals(QWPASUPPLICANT_LIBRARY_TYPE, staticlib) {
    QMAKE_PKGCONFIG_CFLAGS = -DQWPASUPPLICANT_STATIC
} else {
    QMAKE_PKGCONFIG_CFLAGS = -DQWPASUPPLICANT_SHARED
}
unix:QMAKE_CLEAN += -r pkgconfig lib$${TARGET}.prl

OTHER_FILES += \
    doc/dbus/fi.w1.wpa_supplicant1.Interface.xml \
    doc/dbus/fi.w1.wpa_supplicant1.Network.xml \
    doc/dbus/fi.w1.wpa_supplicant1.xml \
    doc/wpa_supplicant/conf/credentials_block.txt \
    doc/wpa_supplicant/conf/interface_block.txt \
    doc/wpa_supplicant/conf/network_block.txt \
    doc/wpa_supplicant/conf/global_block.txt
