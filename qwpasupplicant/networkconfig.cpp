/*
 *
 */

#include <QStringList>
#include "configuration.h"

namespace QWpaSupplicant
{

NetworkConfig::NetworkConfig()
{
}

bool NetworkConfig::enabled() const
{
    return value("enabled").toBool();
}

void NetworkConfig::setEnabled(bool enabled)
{
    insert("enabled",enabled);
}

QString NetworkConfig::idStr() const
{
    return value("id_str").toString();
}

void NetworkConfig::setIdStr(const QString &idStr)
{
    if(idStr.length())
        insert("id_str",idStr);
}

QString NetworkConfig::ssid() const
{
    return value("ssid").toString();
}

void NetworkConfig::setSsid(const QString &ssid)
{
    if(ssid.length())
        insert("ssid",ssid);
}

bool NetworkConfig::scanSsid() const
{
    return value("scan_ssid",false).toBool();
}

void NetworkConfig::setScanSsid(bool scanSsid)
{
    insert("scan_ssid",scanSsid);
}

QString NetworkConfig::bssid() const
{
    return value("bssid").toString();
}

void NetworkConfig::setBssid(const QString &bssid)
{
    if(bssid.length())
        insert("bssid",bssid);
}

int NetworkConfig::priority() const
{
    return value("priorit",0).toInt();
}

void NetworkConfig::setPriority(int priority)
{
    if(priority>=0)
            insert("priority",priority);
}

int NetworkConfig::mode() const
{
    return value("mode",0).toInt();
}

void NetworkConfig::setMode(int mode)
{
    if(mode>=0)
        insert("mode",mode);
}

int NetworkConfig::frequency() const
{
    return value("frequency").toInt();
}

void NetworkConfig::setFrequency(int frequency)
{
    // cheap-checking.
    if((frequency >= 2400 && frequency <= 2500) ||
            (frequency >= 5725 && frequency <=5875))
        insert("frequency",frequency);
}

// scan_freq
// freq_list
// bgscan

NetworkConfig::Protocol NetworkConfig::proto() const
{
    Protocol proto;
    QString setting = value("proto","WPA RSN").toString();
    QStringList settings = setting.split(" ");
    foreach(setting,settings)
    {
        if(setting.compare("WPA")==0)
        {
            proto |= ProtocolWpa;
            continue;
        }
        if(setting.compare("WPA2")==0)
        {
            proto |= ProtocolRsn;
            continue;
        }
        if(setting.compare("RSN")==0)
        {
            proto |= ProtocolRsn;
            continue;
        }
    }

    return proto;
}

void NetworkConfig::setProto(Protocol proto)
{
    QString setting;
    if(proto.testFlag(ProtocolWpa))
        setting.append("WPA ");
    if(proto.testFlag(ProtocolRsn))
        setting.append("RSN ");
    insert("proto",setting.trimmed());
}

NetworkConfig::KeyManagement NetworkConfig::keyMgmt() const
{
    KeyManagement keyMgmt;
    QString setting = value("key_mgmt","WPA-PSK WPA-EAP").toString();
    QStringList settings = setting.split(" ");
    foreach(setting,settings)
    {
        if(setting.compare("WPA-PSK")==0)
        {
            keyMgmt |= KeyWpaPsk ;
            continue;
        }
        if(setting.compare("WPA-PSK-SHA256")==0)
        {
            keyMgmt |= KeyWpaPskSha256 ;
            continue;
        }
        if(setting.compare("WPA-EAP")==0)
        {
            keyMgmt |= KeyWpaEap ;
            continue;
        }
        if(setting.compare("WPA-EAP-SHA256")==0)
        {
            keyMgmt |= KeyWpaEapSha256 ;
            continue;
        }
        if(setting.compare("IEEE8021X")==0)
        {
            keyMgmt |= KeyIeee8021X ;
            continue;
        }
        if(setting.compare("NONE")==0)
        {
            keyMgmt |= KeyNone ;
            continue;
        }
    }

    return keyMgmt;
}

void NetworkConfig::setKeyMgmt(KeyManagement keyMgmt)
{
    QString setting;
    if(keyMgmt.testFlag(KeyWpaPsk))
        setting.append("WPA-PSK ");
    if(keyMgmt.testFlag(KeyWpaPskSha256))
        setting.append("WPA-PSK-SHA256 ");
    if(keyMgmt.testFlag(KeyWpaEap))
        setting.append("WPA-EAP ");
    if(keyMgmt.testFlag(KeyWpaEapSha256))
        setting.append("WPA-PSK-SHA256 ");
    if(keyMgmt.testFlag(KeyIeee8021X))
        setting.append("IEEE8021X ");
    if(keyMgmt.testFlag(KeyNone))
        setting.append("NONE ");
    insert("key_mgmt",setting.trimmed());
}

// ieee80211w

NetworkConfig::AuthenticationAlgorithm NetworkConfig::authAlg() const
{
    AuthenticationAlgorithm authAlg;
    QString setting = value("auth_alg").toString();
    QStringList settings = setting.split(" ");
    foreach(setting,settings)
    {
        if(setting.compare("OPEN")==0)
        {
            authAlg |= AuthOpen;
            continue;
        }
        if(setting.compare("SHARED")==0)
        {
            authAlg |= AuthShared;
            continue;
        }
        if(setting.compare("LEAP")==0)
        {
            authAlg |= AuthLeap;
            continue;
        }
    }
    return authAlg;
}

void NetworkConfig::setAuthAlg(AuthenticationAlgorithm authAlg)
{
    QString setting;
    if(authAlg.testFlag(AuthOpen))
        setting.append("OPEN ");
    if(authAlg.testFlag(AuthShared))
        setting.append("SHARED ");
    if(authAlg.testFlag(AuthLeap))
        setting.append("LEAP ");
    insert("auth_alg",setting.trimmed());
}

NetworkConfig::Pairwise NetworkConfig::pairwise() const
{
    Pairwise pairwise;
    QString setting = value("pairwise","CCMP TKIP").toString();
    QStringList settings = setting.split(" ");
    foreach(setting,settings)
    {
        if(setting.compare("CCMP")==0)
        {
            pairwise |= PairwiseCcmp;
            continue;
        }
        if(setting.compare("TKIP")==0)
        {
            pairwise |= PairwiseTkip;
            continue;
        }
        if(setting.compare("NONE")==0)
        {
            pairwise |= PairwiseNone;
            continue;
        }
    }
    return pairwise;
}

void NetworkConfig::setPairwise(Pairwise pairwise)
{
    QString setting;
    if(pairwise.testFlag(PairwiseCcmp))
        setting.append("CCMP ");
    if(pairwise.testFlag(PairwiseTkip))
        setting.append("TKIP ");
    if(pairwise.testFlag(PairwiseNone))
        setting.append("NONE ");
    insert("pairwise",setting.trimmed());
}

NetworkConfig::GroupCipher NetworkConfig::group() const
{
    GroupCipher group;
    QString setting = value("group","CCMP TKIP WEP104 WEP40").toString();
    QStringList settings = setting.split(" ");
    foreach(setting,settings)
    {
        if(setting.compare("CCMP"))
        {
            group |= GroupCipherCcmp;
            continue;
        }
        if(setting.compare("TKIP"))
        {
            group |= GroupCipherTkip;
            continue;
        }
        if(setting.compare("WEP104"))
        {
            group |= GroupCipherWep104;
            continue;
        }
        if(setting.compare("WEP40"))
        {
            group |= GroupCipherWep40;
            continue;
        }
    }
    return group;
}

void NetworkConfig::setGroup(GroupCipher group)
{
    QString setting;
    if(group.testFlag(GroupCipherCcmp))
        setting.append("CCMP ");
    if(group.testFlag(GroupCipherTkip))
        setting.append("TKIP ");
    if(group.testFlag(GroupCipherWep104))
        setting.append("WEP104 ");
    if(group.testFlag(GroupCipherWep40))
        setting.append("WEP40 ");

    insert("group",setting.trimmed());
}

QString NetworkConfig::psk() const
{
    return value("psk").toString();
}

void NetworkConfig::setPsk(const QString &psk)
{
    if(!psk.isEmpty())
        insert("psk",psk);
}

NetworkConfig::EapolFlags NetworkConfig::eapolFlags() const
{
    return EapolFlags(value("eapol_flags").toInt());
}

void NetworkConfig::setEapolFlags(EapolFlags eapolFlags)
{
    insert("eapol_flags",eapolFlags);
}

// macsec_policy
// mixed_cell
// proactive_key_caching

QString NetworkConfig::wepKey0() const
{
    return value("wep_key0").toString();
}

void NetworkConfig::setWepKey0(const QString &wepKey0)
{
    if(!wepKey0.isEmpty())
        insert("wep_key0",wepKey0);
}

QString NetworkConfig::wepKey1() const
{
    return value("wep_key1").toString();
}

void NetworkConfig::setWepKey1(const QString &wepKey1)
{
    if(!wepKey1.isEmpty())
        insert("wep_key1",wepKey1);
}

QString NetworkConfig::wepKey2() const
{
    return value("wep_key2").toString();
}

void NetworkConfig::setWepKey2(const QString &wepKey2)
{
    if(!wepKey2.isEmpty())
        insert("wep_key2",wepKey2);
}

QString NetworkConfig::wepKey3() const
{
    return value("wep_key3").toString();
}

void NetworkConfig::setWepKey3(const QString &wepKey3)
{
    if(!wepKey3.isEmpty())
        insert("wep_key3",wepKey3);
}

uint NetworkConfig::wepTxKeyIdx() const
{
    return value("wep_tx_keyidx").toUInt();
}

void NetworkConfig::setWepTxKeyIdx(uint wepTxKeyIdx)
{
    if(wepTxKeyIdx <= 3)
        insert("wep_tx_keyidx",wepTxKeyIdx);
}

bool NetworkConfig::peerkey() const
{
    return value("peerkey").toBool();
}

void NetworkConfig::setPeerkey(bool peerkey)
{
    insert("peerkey",peerkey);
}

int NetworkConfig::wpaPtkRekey() const
{
    return value("wpa_ptk_reky").toInt();
}

void NetworkConfig::setWpaPtkRekey(int wpaPtkRekey)
{
    if(wpaPtkRekey>=0)
        insert("wpa_ptk_rekey",wpaPtkRekey);
}

NetworkConfig::EapMethod NetworkConfig::eap() const
{
    EapMethod eap;
    QString setting = value("eap","MD5 MSCHAPV2 OTP GTC TLS PEAP TTLS").toString();
    QStringList settings = setting.split(" ");
    foreach(setting,settings)
    {
        if(setting.compare("MD5")==0)
        {
            eap |= EapMethodMd5;
            continue;
        }
        if(setting.compare("MSCHAPV2")==0)
        {
            eap |= EapMethodMsChapV2;
            continue;
        }
        if(setting.compare("OTP")==0)
        {
            eap |= EapMethodOtp;
            continue;
        }
        if(setting.compare("GTC")==0)
        {
            eap |= EapMethodGtc;
            continue;
        }
        if(setting.compare("TLS")==0)
        {
            eap |= EapMethodTls;
            continue;
        }
        if(setting.compare("PEAP")==0)
        {
            eap |= EapMethodPeap;
            continue;
        }
        if(setting.compare("TTLS")==0)
        {
            eap |= EapMethodTtls;
            continue;
        }
    }
    return eap;
}

void NetworkConfig::setEap(EapMethod eap)
{
    QString setting;
    if(eap.testFlag(EapMethodMd5))
        setting.append("MD5 ");
    if(eap.testFlag(EapMethodMsChapV2))
        setting.append("MSCHAPV2 ");
    if(eap.testFlag(EapMethodOtp))
        setting.append("OTP ");
    if(eap.testFlag(EapMethodGtc))
        setting.append("GTC ");
    if(eap.testFlag(EapMethodTls))
        setting.append("TLS ");
    if(eap.testFlag(EapMethodPeap))
        setting.append("PEAP ");
    if(eap.testFlag(EapMethodTtls))
        setting.append("TTLS ");
    insert("eap",setting.trimmed());
}

QString NetworkConfig::identity() const
{
    return value("identity").toString();
}

void NetworkConfig::setIdentity(const QString &identity)
{
    if(!identity.isEmpty())
        insert("identity",identity);
}

QString NetworkConfig::anonymousIdentity() const
{
    return value("anonymous_identity").toString();
}

void NetworkConfig::setAnonymousIdentity(const QString &anonymousIdentity)
{
    if(!anonymousIdentity.isEmpty())
        insert("anonymous_identity",anonymousIdentity);
}

QString NetworkConfig::password() const
{
    return value("password").toString();
}

void NetworkConfig::setPassword(const QString &password)
{
    if(!password.isEmpty())
        insert("password",password);
}

QString NetworkConfig::caCert() const
{
    return value("ca_cert").toString();
}

void NetworkConfig::setCaCert(const QString &caCert)
{
    if(!caCert.isEmpty())
        insert("ca_cert",caCert);
}

QString NetworkConfig::caPath() const
{
    return value("ca_path").toString();
}

void NetworkConfig::setCaPath(const QString &caPath)
{
    if(!caPath.isEmpty())
        insert("ca_path",caPath);
}

QString NetworkConfig::clientCert() const
{
    return value("client_cert").toString();
}

void NetworkConfig::setClientCert(const QString &clientCert)
{
    if(!clientCert.isEmpty())
        insert("client_cert",clientCert);
}

QString NetworkConfig::privateKey() const
{
    return value("private_key").toString();
}

void NetworkConfig::setPrivateKey(const QString &privateKey)
{
    if(!privateKey.isEmpty())
        insert("private_key",privateKey);
}

QString NetworkConfig::privateKeyPassword() const
{
    return value("private_key_passwd").toString();
}

void NetworkConfig::setPrivateKeyPassword(const QString &privateKeyPassword)
{
    if(!privateKeyPassword.isEmpty())
        insert("private_key_passwd",privateKeyPassword);
}

QString NetworkConfig::dhFile() const
{
    return value("dh_file").toString();
}

void NetworkConfig::setDhFile(const QString &dhFile)
{
    if(!dhFile.isEmpty())
        insert("dh_file",dhFile);
}

QString NetworkConfig::subjectMatch() const
{
    return value("subject_match").toString();
}

void NetworkConfig::setSubjectMatch(const QString &subjectMatch)
{
    if(!subjectMatch.isEmpty())
        insert("subject_match",subjectMatch);
}

QString NetworkConfig::altSubjectMatch() const
{
    return value("alt_subject_match").toString();
}

void NetworkConfig::setAltSubjectMatch(const QString &altSubjectMatch)
{
    if(!altSubjectMatch.isEmpty())
        insert("alt_subject_match",altSubjectMatch);
}

QString NetworkConfig::phase1() const
{
    return value("phase1").toString();
}

void NetworkConfig::setPhase1(const QString &phase1)
{
    if(!phase1.isEmpty())
        insert("phase1",phase1);
}

QString NetworkConfig::phase2() const
{
    return value("phase2").toString();
}

void NetworkConfig::setPhase2(const QString &phase2)
{
    if(!phase2.isEmpty())
        insert("phase2",phase2);
}

// tls_allow_md5
// tls_disable_time_checks
// tls_disable_session_ticket
// tls_disable_tlsv1_1
// tls_disable_tlsv1_2

// These are used for inner Phase2 auth e/ EAP-TTLS/EAP-PEAP
QString NetworkConfig::caCert2() const
{
    return value("ca_cert2").toString();
}

void NetworkConfig::setCaCert2(const QString &caCert)
{
    if(!caCert.isEmpty())
        insert("ca_cert2",caCert);
}

QString NetworkConfig::caPath2() const
{
    return value("ca_path2").toString();
}

void NetworkConfig::setCaPath2(const QString &caPath)
{
    if(!caPath.isEmpty())
        insert("ca_path2",caPath);
}

QString NetworkConfig::clientCert2() const
{
    return value("client_cert2").toString();
}

void NetworkConfig::setClientCert2(const QString &clientCert)
{
    if(!clientCert.isEmpty())
        insert("client_cert2",clientCert);
}

QString NetworkConfig::privateKey2() const
{
    return value("private_key2").toString();
}

void NetworkConfig::setPrivateKey2(const QString &privateKey)
{
    if(!privateKey.isEmpty())
        insert("private_key2",privateKey);
}

QString NetworkConfig::privateKey2Password() const
{
    return value("private_key2_passwd").toString();
}

void NetworkConfig::setPrivateKey2Password(const QString &privateKeyPassword)
{
    if(!privateKeyPassword.isEmpty())
        insert("private_key2_passwd",privateKeyPassword);
}

QString NetworkConfig::dhFile2() const
{
    return value("dh_file2").toString();
}

void NetworkConfig::setDhFile2(const QString &dhFile)
{
    if(!dhFile.isEmpty())
        insert("dh_file2",dhFile);
}

QString NetworkConfig::subjectMatch2() const
{
    return value("subject_match2").toString();
}

void NetworkConfig::setSubjectMatch2(const QString &subjectMatch)
{
    if(!subjectMatch.isEmpty())
        insert("subject_match2",subjectMatch);
}

QString NetworkConfig::altSubjectMatch2() const
{
    return value("alt_subject_match2").toString();
}

void NetworkConfig::setAltSubjectMatch2(const QString &altSubjectMatch)
{
    if(!altSubjectMatch.isEmpty())
        insert("alt_subject_match2",altSubjectMatch);
}

uint NetworkConfig::fragementSize() const
{
    return value("fragment_size",1398).toUInt();
}

void NetworkConfig::setFragmentSize(uint fragmentSize)
{
    if(fragmentSize > 0)
        insert("fragment_size",fragmentSize);
}

OcspPolicy NetworkConfig::ocsp() const
{
    return OcspPolicy(value("ocsp").toInt());
}

void NetworkConfig::setOcsp(OcspPolicy ocsp)
{
    insert("ocsp",uint(ocsp));
}

QString NetworkConfig::openSslCiphers() const
{
    return value("openssl_ciphers").toString();
}

void NetworkConfig::setOpenSslCiphers(const QString &ciphers)
{
    if(!ciphers.isEmpty())
        insert("openssl_ciphers",ciphers);
}

bool NetworkConfig::erp() const
{
    return value("erp",true).toBool();
}

void NetworkConfig::setErp(bool enabled)
{
    insert("erp",enabled);
}

// EAP-FAST variables:
QString NetworkConfig::pacFile() const
{
    return value("pac_file").toString();
}

void NetworkConfig::setPacFile(const QString &pacFile)
{
    if(!pacFile.isEmpty())
        insert("pac_file",pacFile);
}

// EAP-FAST has it's own phase1 .. but I fail in understanding.
// EAP-FAST: fast_max_pac_list_len
// EAP-FAST: fast_pac_format

MacAddressPolicy NetworkConfig::macAddr() const
{
    return MacAddressPolicy(value("mac_addr",MacAddressPermanent).toInt());
}

void NetworkConfig::setMacAddr(MacAddressPolicy macAddrPolicy)
{
    insert("mac_addr",macAddrPolicy);
}

bool NetworkConfig::disableHt() const
{
    return value("disable_ht",false).toBool();
}

void NetworkConfig::setDisableHt(bool disable)
{
    insert("disable_ht",disable);
}

bool NetworkConfig::disableHt40() const
{
    return value("disable_ht40",false).toBool();
}

void NetworkConfig::setDisableHt40(bool disable)
{
    insert("disable_ht40",disable);
}

bool NetworkConfig::disableSgi() const
{
    return value("disable_sgi",false).toBool();
}
void NetworkConfig::setDisableSgi(bool disable)
{
    insert("disable_sgi",disable);
}

bool NetworkConfig::disableLpdc() const
{
    return value("disable_lpdc",false).toBool();
}
void NetworkConfig::setDisableLpdc(bool disable)
{
    insert("disable_lpdc",disable);
}

bool NetworkConfig::ht40Intolerant() const
{
    return value("ht40_intolerant",false).toBool();
}

void NetworkConfig::setHt40Intolerant(bool enable)
{
    insert("ht40_intolerant",enable);
}

// ht_mcs : Parsed as an array of bytes, in base-16 (ascii-hex) [later]
// disable_max_amsdu:  Whether MAX_AMSDU should be disabled.
// ampdu_factor: Maximum A-MPDU Length Exponent
// ampdu_density:  Allow overriding AMPDU density configuration

bool NetworkConfig::disableVht() const
{
    return value("disable_vht",false).toBool();
}

void NetworkConfig::setDisableVht(bool disable)
{
    insert("disable_vht",disable);
}

// vht_capa: VHT capabilities to set in the override
// vht_capa_mask: mask of VHT capabilities
// vht_rx_mcs_nss_1/2/3/4/5/6/7/8: override the MCS set for RX NSS 1-8
// vht_tx_mcs_nss_1/2/3/4/5/6/7/8: override the MCS set for TX NSS 1-8
} // EON
// EOF
