#ifndef SUPPLICANTHANDLER_H
#define SUPPLICANTHANDLER_H

#include <QObject>
#include <QtDBus>
#include <QDBusConnection>
#include <QDBusInterface>
#include <QDBusReply>
#include <QDebug>
#include "qwpasupplicant/configuration.h"
#include "qwpasupplicant/dbus/wpasupplicant.h"

class SupplicantHandler : public QObject
{
    Q_OBJECT
    Q_PROPERTY(QWpaSupplicant::InterfaceConfig InterfaceConfig READ interfaceConfig WRITE setInterfaceConfig)
    Q_PROPERTY(QWpaSupplicant::NetworkConfig NetworkConfig READ networkConfig WRITE setNetworkConfig)

public:
    SupplicantHandler(QObject *parent = 0);

    QWpaSupplicant::InterfaceConfig interfaceConfig() const;
    void setInterfaceConfig(const QWpaSupplicant::InterfaceConfig &value);
    QWpaSupplicant::NetworkConfig networkConfig() const;
    void setNetworkConfig(const QWpaSupplicant::NetworkConfig &value);

private:
    fi::w1::wpa_supplicant1::wpa_supplicant1 *m_wpaSupplicant;
    fi::w1::wpa_supplicant1::Interface *m_wpaInterface;
    fi::w1::wpa_supplicant1::Network *m_wpaNetwork;
    QWpaSupplicant::InterfaceConfig m_interfaceConfig;
    QWpaSupplicant::NetworkConfig m_networkConfig;

    void initializeData();
    bool initializeSupplicant();
    bool initializeInterface();
    bool purgeNetworksFromInterface();
    void setWiredKeys();

Q_SIGNALS:
    void StateChanged(const QString &newState);
    void ConfigChanged();

public Q_SLOTS:
    void run();

private Q_SLOTS:
    // for supplicant
    void interfaceAdded(const QDBusObjectPath &path, const QVariantMap &properties);
    // for interface
    void networkAdded(const QDBusObjectPath &path, const QVariantMap &properties);
    void networkRemoved(const QDBusObjectPath &path);
    void networkSelected(const QDBusObjectPath &path);
    void eap(const QString &status, const QString &parameter);
    void staAuthorized(const QString &name);
    void staDeauthorized(const QString &name);
    void propertiesChanged(const QVariantMap &properties);
};

#endif // SUPPLICANTHANDLER_H
