#-------------------------------------------------
#
# Project created by QtCreator 2014-12-01T15:30:27
#
#-------------------------------------------------

include(../qwpasupplicant.pri)

QT += core
QT += dbus
QT -= gui

TARGET = ../qt-supplicant
CONFIG += console

CONFIG -= app_bundle

TEMPLATE = app
DEPENDPATH += .
INCLUDEPATH += $${QWPASUPPLICANT_INCLUDEPATH}
LIBS += -L../lib $${QWPASUPPLICANT_LIBS}

SOURCES += \
    main.cpp \
    supplicanthandler.cpp

HEADERS += \
    supplicanthandler.h


