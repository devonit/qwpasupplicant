#include "supplicanthandler.h"

SupplicantHandler::SupplicantHandler(QObject *parent) :
    QObject(parent),
    m_wpaSupplicant(0),
    m_wpaInterface(0),
    m_wpaNetwork(0)
{
}

QWpaSupplicant::InterfaceConfig SupplicantHandler::interfaceConfig() const
{
    return m_interfaceConfig;
}

void SupplicantHandler::setInterfaceConfig(const QWpaSupplicant::InterfaceConfig &config)
{
    m_interfaceConfig = config;
}

QWpaSupplicant::NetworkConfig SupplicantHandler::networkConfig() const
{
    return m_networkConfig;
}

void SupplicantHandler::setNetworkConfig(const QWpaSupplicant::NetworkConfig &config)
{
    m_networkConfig = config;
}

void SupplicantHandler::run()
{
    // do we have what we need?


    // can we talk to supplicant?
    if(!initializeSupplicant())
    {
        qDebug() << "Failed to get supplicant";
        return;
    }


    if(!initializeInterface()){
        qDebug() << "Failed to get interface";
        return;
    }
    // lets listen for some signals!
    connect(m_wpaInterface,SIGNAL(NetworkAdded(QDBusObjectPath,QVariantMap)),this,SLOT(networkAdded(QDBusObjectPath,QVariantMap)));
    connect(m_wpaInterface,SIGNAL(NetworkSelected(QDBusObjectPath)),this,SLOT(networkSelected(QDBusObjectPath)));
    connect(m_wpaInterface,SIGNAL(NetworkRemoved(QDBusObjectPath)),this,SLOT(networkRemoved(QDBusObjectPath)));
    connect(m_wpaInterface,SIGNAL(PropertiesChanged(QVariantMap)),this,SLOT(propertiesChanged(QVariantMap)));
    // Onward!
    if(!purgeNetworksFromInterface()){
        qDebug() << "Failed to purge networks";
        return;
    }
    // K, we're at "scratch" on this network. now set what we need.
    QDBusPendingReply<QDBusObjectPath> pr = m_wpaInterface->AddNetwork(m_networkConfig);
    pr.waitForFinished();
    if(pr.isError()){
        qDebug() << "Error AddNetwork";
        qDebug() << "Last Error: " << QDBusConnection::systemBus().lastError();
        return;
    }else{
        qDebug() << "Successfully Added network.";
    }
    QDBusObjectPath networkPath = pr.value();
    m_wpaInterface->SelectNetwork(networkPath);
    //m_wpaInterface->EAPLogon();
}

bool SupplicantHandler::initializeSupplicant(){
    // connect to wpa_supplicant.
    m_wpaSupplicant = new fi::w1::wpa_supplicant1::wpa_supplicant1(
                WPA_SUPPLICANT_SERVICE,
                WPA_SUPPLICANT_SERVICE_PATH,
                QDBusConnection::systemBus()
                );
    if( !m_wpaSupplicant->isValid() ){
        qDebug() << "Error init supplicant";
        qDebug() << "Last Error: " << QDBusConnection::systemBus().lastError();
        return false;
    }
    return true;
}

bool SupplicantHandler::initializeInterface(){
    // we need to have a valid m_wpaSupplicant
    if(!m_wpaSupplicant->isValid()){
        qDebug() << "No connection to wpa_supplicant!";
        return false;
    }
    if(m_interfaceConfig.ifname().length() < 1){
        qDebug() << "No interface defined!" << m_interfaceConfig;
        return false;
    }
    // ask wpa_supplicant for the interface by name
    QDBusPendingReply<QDBusObjectPath> pr = m_wpaSupplicant->GetInterface(m_interfaceConfig.ifname());
    pr.waitForFinished();
    if( pr.isValid() && !pr.isError()){
        qDebug() << "m_interfaceConfig.ifname" << m_interfaceConfig.ifname() << pr.value().path() ;
    }else{
        qDebug() << "Error GetInterface Error:" <<  pr.error().name() << pr.error().message();
        if(pr.error().name() != "fi.w1.wpa_supplicant1.InterfaceUnknown"){
            return false;
        }else{
            // this interface is unknown. Attempt to add it.
            qDebug() <<"Attempting CreateInterface" ;
            // this takes a{sv} (http://w1.fi/wpa_supplicant/devel/dbus.html)
            pr = m_wpaSupplicant->CreateInterface(m_interfaceConfig);
            pr.waitForFinished();
            if(pr.isError()){
                qDebug() << "Error CreateInterface Error:" << m_interfaceConfig.ifname() << pr.error().name() << pr.error().message();
                return false;
            }
        }
    }
    // get the actual Interface
    m_wpaInterface = new fi::w1::wpa_supplicant1::Interface(
                WPA_SUPPLICANT_SERVICE,
                pr.value().path(),
                QDBusConnection::systemBus()
                );
    if( !m_wpaInterface->isValid() ){
        qDebug() << "Error init interface";
        qDebug() << "Last Error: " << QDBusConnection::systemBus().lastError();
        return false;
    }

    // we "should" be good to go from here. Set those keys we can..
    m_wpaInterface->setApScan(m_interfaceConfig.apScan());
    m_wpaInterface->setFastReauth(m_interfaceConfig.fastReauth());


    return true;
}

bool SupplicantHandler::purgeNetworksFromInterface(){
    // we need to have the interface, and then call RemoveAllNetworks on it
    if(!m_wpaInterface->isValid()){
        qDebug() << "Not connected to the interface for " << m_interfaceConfig.ifname();
        return false;
    }
    if(m_wpaInterface->ifname() != m_interfaceConfig.ifname()){
        qDebug() << "wpaInterface doesn't match interface!" << m_wpaInterface->ifname() << m_interfaceConfig.ifname();
        return false;
    }

    m_wpaInterface->RemoveAllNetworks();

    return true;
}

// for supplicant
void SupplicantHandler::interfaceAdded(const QDBusObjectPath &path, const QVariantMap &properties){
    qDebug() << "InterfaceAdded" << path.path();
}

// for interface
void SupplicantHandler::networkAdded(const QDBusObjectPath &path, const QVariantMap &properties){
    qDebug() << "NetworkAdded" << path.path();
}

void SupplicantHandler::networkRemoved(const QDBusObjectPath &path){
    qDebug() << "NetworkRemoved" << path.path();
}

void SupplicantHandler::networkSelected(const QDBusObjectPath &path){
    qDebug() << "NetworkSelected" << path.path();
}

void SupplicantHandler::eap(const QString &status, const QString &parameter){
    qDebug() << "EAP" << status << parameter;
}

void SupplicantHandler::staAuthorized(const QString &name){
    qDebug() << "StaAuthorized" << name;
}

void SupplicantHandler::staDeauthorized(const QString &name){
    qDebug() << "StaDeauthorized" << name;
}

void SupplicantHandler::propertiesChanged(const QVariantMap &properties){
    qDebug() << "PropertiesChanged";
    foreach (QString key, properties.keys()){
        QString prop = properties[key].toString();
        qDebug() << "\t" << key << prop;
        if(key == "State"){
            Q_EMIT(StateChanged(prop));
        }
    }
}
