#include <QCoreApplication>
#include <QDebug>
#include "supplicanthandler.h"
#include "qwpasupplicant/configuration.h"

int main(int argc, char *argv[]){
    QCoreApplication app(argc,argv);

    QWpaSupplicant::InterfaceConfig ifConfig;

    ifConfig.setIfname("eth0");
    ifConfig.setDriver(QWpaSupplicant::InterfaceConfig::DriverWired);
    ifConfig.setApScan(QWpaSupplicant::ApScanDriver);

    QWpaSupplicant::NetworkConfig netConfig;

    netConfig.setKeyMgmt(QWpaSupplicant::NetworkConfig::KeyIeee8021X);
    netConfig.setIdentity("bob");
    netConfig.setPassword("hello");


    SupplicantHandler *handler = new SupplicantHandler;
    handler->setInterfaceConfig(ifConfig);
    handler->setNetworkConfig(netConfig);
    handler->run();

    app.exec();
}
